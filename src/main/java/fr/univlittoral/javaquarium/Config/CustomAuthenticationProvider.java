package fr.univlittoral.javaquarium.Config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.univlittoral.javaquarium.DO.UtilisateurDO;
import fr.univlittoral.javaquarium.DO.PasswordDO;
import fr.univlittoral.javaquarium.BO.UtilisateurBO;

@Component
@Transactional
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private UtilisateurBO utilisateurBO;

	@Autowired
	private PasswordDO passwordBO;

	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
		final String userName = authentication.getName();
		final String password = authentication.getCredentials().toString();

		final List<UtilisateurDO> utilisateurEntity = utilisateurBO.findByLoginOrEmail(userName);
		if (utilisateurEntity != null && passwordBO.matches(password, utilisateurEntity.getPassword())) {

			// Création d'un bean perso pour ajouter des valeurs.
			final List<GrantedAuthority> grantedAuths = new ArrayList<>();
			final PortailSpringUser principal = new PortailSpringUser(utilisateurEntity.getId(), userName, password, true, true, true, true, grantedAuths);

			return new UsernamePasswordAuthenticationToken(principal, password, grantedAuths);
		}

		// Arrivé ici alors KO.
		return null;
	}

	@Override
	public boolean supports(final Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}

