package fr.univlittoral.javaquarium.Config;

import org.springframework.security.core.Authentication;

public interface IAuthenticationFacade {

	/**
	 * Retrieve the user details in Spring Security.
	 *
	 * @return
	 */
	Authentication getAuthentication();

}
