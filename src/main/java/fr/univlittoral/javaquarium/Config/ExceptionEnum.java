package fr.univlittoral.javaquarium.Config;

public enum ExceptionEnum {
	// @formatter:off

		// Login
		IDENTIFIANT_NULL,
		MOT_DE_PASSE_NULL,
		UTILISATEUR_INEXISTANT,
		UTILISATEUR_DESACTIVE,
		UTILISATEUR_SUPPRIME,
		BAD_CREDENTIAL,
		
		// Structure
		STRUCTURE_HAS_CHILD,

		// Gestion authentification
		TOKEN_EXPIRED,
		TOKEN_MISSING,

		// Données non trouvées en BDD
		USER_NOT_FOUND,
		STRUCTURE_NOT_FOUND,
		POSTE_NOT_FOUND,
		FONCTION_NOT_FOUND,

		// Autre
		USER_DISABLED,
		POSTE_ALREADY_EXIST;

		// @formatter:on


}
