package fr.univlittoral.javaquarium.Config;


public class BadCredentialException extends RestException {

	private static final long serialVersionUID = -7714927956026614197L;

	/**
	 * Const
	 */
	public BadCredentialException() {
		setBusinessCode(ExceptionEnum.BAD_CREDENTIAL);
	}

}
