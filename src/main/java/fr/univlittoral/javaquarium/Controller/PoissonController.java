package fr.univlittoral.javaquarium.Controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univlittoral.javaquarium.BO.PoissonBO;
import fr.univlittoral.javaquarium.DO.PoissonDO;
import fr.univlittoral.javaquarium.DTO.PoissonDTO;

@RestController
@RequestMapping(value = "/poissonController")
@Transactional
public class PoissonController {

	@Autowired
	private PoissonBO poissonBO;
	
	/**
	 * Méthode permettant de convertir les poissonDo en poissonDTO pour pouvoir les afficher
	 * @return Collection<PoissonDTO>
	 */
	@RequestMapping(value = "/listePoissons", method = RequestMethod.GET)
	public Collection<PoissonDTO> getAllPoissons(){
	
		final List<PoissonDO> poissons = poissonBO.findAll();
		
		final Collection<PoissonDTO> dto = new ArrayList();
		
		for (final PoissonDO poissonDO : poissons) {
			final PoissonDTO poissonDTO = new PoissonDTO();
			poissonDTO.setNom(poissonDO.getEspece());
			poissonDTO.setDescription(poissonDO.getDescription1());
			poissonDTO.setCouleur(poissonDO.getCouleur());
			poissonDTO.setPrix(poissonDO.getPrix());
			poissonDTO.setLargeur(poissonDO.getLargeur());
			poissonDTO.setLongueur(poissonDO.getLongueur());
			
			dto.add(poissonDTO);
		}
		
		return dto;
	}
	
	/**
	 * Méthode permettant de récupérer un poissonDTO et de le convertir en PoissonDO
	 * @param poissonDTO
	 */
	@RequestMapping(value = "/addPoisson", method = RequestMethod.POST)
	public void addPoisson(final PoissonDTO poissonDTO) {
		//mapper comme au dessus (envoyer un DO au back)
		
		final PoissonDO newPoissonDO = new PoissonDO();
		newPoissonDO.setCouleur(poissonDTO.getCouleur());
		newPoissonDO.setEspece(poissonDTO.getNom());
		newPoissonDO.setDescription1(poissonDTO.getDescription());
		newPoissonDO.setPrix(poissonDTO.getPrix());
		newPoissonDO.setLongueur(poissonDTO.getLongueur());
		newPoissonDO.setLargeur(poissonDTO.getLargeur());
		
		poissonBO.newPoissonBO(newPoissonDO);
		
	}
	
}
