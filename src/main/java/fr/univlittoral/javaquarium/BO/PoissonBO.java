package fr.univlittoral.javaquarium.BO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univlittoral.javaquarium.DAO.PoissonDAO;
import fr.univlittoral.javaquarium.DO.PoissonDO;

@Service
public class PoissonBO {
	
	@Autowired
	private PoissonDAO dao;
	
	/**
	 * Constructeur PoissonBO, initialisation du poissonDAO
	 */
	public PoissonBO() {
		dao = new PoissonDAO();
	}
	
	/**
	 * Récupère la liste de tous les poissons (PoissonDO)
	 * @return List<PoissonDO>
	 */
	public List<PoissonDO> findAll(){
		return dao.findAll();
	}
	
	/**
	 * Méthode qui récupère le PoissonDO qui va être créer
	 * @param request
	 */
	public void newPoissonBO(final PoissonDO request) {
		dao.addPoisson(request);
	}
}
