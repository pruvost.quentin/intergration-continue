package fr.univlittoral.javaquarium.BO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univlittoral.javaquarium.DAO.UtilisateurDAO;
import fr.univlittoral.javaquarium.DO.UtilisateurDO;

public class UtilisateurBO {

	@Autowired
	private UtilisateurDAO dao;
	
	/**
	 * Constructeur UtilisateurBO, initialisation du utilisateurDAO
	 */
	public UtilisateurBO() {
		dao = new UtilisateurDAO();
	}
	
	/**
	 * Récupère la liste de tous les utilisateurs (UtilisateurDO)
	 * @return List<UtilisateurDO>
	 */
	public List<UtilisateurDO> findAll(){
		return dao.findAll();
	}
	
	/**
	 * Récupère un UtilisateurDO
	 * @return UtilisateurDO
	 */
	public List<UtilisateurDO> findByLoginOrEmail(String username){
		return dao.findByLoginOrEmail(username);
	}
	
	
}
