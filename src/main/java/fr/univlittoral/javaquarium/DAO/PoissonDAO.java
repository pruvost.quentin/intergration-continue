package fr.univlittoral.javaquarium.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.univlittoral.javaquarium.DO.PoissonDO;

@Component
public class PoissonDAO {

	@Autowired
	private EntityManager em;
	
	/**
	 * Fonction contenant la requête de récupération de tous les poissons
	 * @return List<PoissonDO>
	 */
	public List<PoissonDO> findAll(){
		
		final String hql = "FROM PoissonDO";
		
		Query query  = em.createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<PoissonDO> list = query.getResultList();
		
		return list;
	};
	
	/**
	 * Fonction contenant l'enregistrement du PoissonDO en bdd
	 * @param request
	 */
	public void addPoisson(PoissonDO request) {
		
		em.persist(request);
		em.flush();
	}
	
}
