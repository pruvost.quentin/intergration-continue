package fr.univlittoral.javaquarium.DAO;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.univlittoral.javaquarium.DO.UtilisateurDO;;

public class UtilisateurDAO {

	@Autowired
	private EntityManager em;
	
	/**
	 * Fonction contenant la requête de récupération de tous utilisateurs
	 * @return List<UtilisateurDO>
	 */
	public List<UtilisateurDO> findAll(){
		
		final String hql = "FROM UtilisateurDO";
		
		Query query  = em.createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<UtilisateurDO> list = query.getResultList();
		
		return list;
	};
	
	/**
	 * Fonction contenant la requête de récupération de l'utilisateur
	 * @return UtilisateurDO
	 */
	public List<UtilisateurDO> findByLoginOrEmail(String username){
		
		final String hql = "FROM UtilisateurDO";
		
		Query query  = em.createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<UtilisateurDO> user = query.getResultList();
		
		return user;
	};
	
	
}
