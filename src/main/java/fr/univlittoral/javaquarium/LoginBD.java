package fr.univlittoral.javaquarium;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/public/bd/login")
@Transactional
public class LoginBD {

	private static final Logger logger = LoggerFactory.getLogger(LoginBD.class);

	@Autowired
	private AuthenticationProvider authenticationManager;

	@Autowired
	private UtilisateurMapper utilisateurMapper;

	/**
	 * methode de connexion d'un utilisateur
	 *
	 * @param request données necessaire a la connexion
	 * @return
	 * @throws RestException
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST)
	public UtilisateurDTO login(@RequestBody final LoginRequestDTO request, final HttpServletRequest req) throws RestException {

		// Controle des params obligatoires
		if (StringUtils.isEmpty(request.getIdentifiant()) || StringUtils.isEmpty(request.getMotDePasse())) {
			throw new BadCredentialException();

		}
		final Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(request.getIdentifiant(), request.getMotDePasse()));
		if (authentication == null) {
			throw new BadCredentialException();
		}

		final PortailSpringUser utilisateur = (PortailSpringUser) authentication.getPrincipal();
		logger.debug("New user logged : " + utilisateur.getUsername());

		return utilisateurMapper.map(utilisateur);
	}

}
