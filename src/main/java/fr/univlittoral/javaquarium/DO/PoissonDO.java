package fr.univlittoral.javaquarium.DO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="P_POISSONS")
public class PoissonDO {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="P_ID")
	private Integer id;
	
	@Column(name="P_ESPECE", nullable=false)
	private String espece;
	
	@Column(name="P_DESC1", nullable=false)
	private String description1;

	@Column(name="P_DESC2", nullable=true)
	private String description2;

	@Column(name="P_DESC3", nullable=true)
	private String description3;
	
	@Column(name="P_COULEUR", nullable=false)
	private String couleur;

	@Column(name="P_LARGEUR", nullable=false)
	private Double largeur;

	@Column(name="P_LONGUEUR", nullable=false)
	private Double longueur;

	@Column(name="P_PRIX", nullable=false)
	private Double prix;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEspece() {
		return espece;
	}

	public void setEspece(String espece) {
		this.espece = espece;
	}

	public String getDescription1() {
		return description1;
	}

	public void setDescription1(String description1) {
		this.description1 = description1;
	}

	public String getDescription2() {
		return description2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	public String getDescription3() {
		return description3;
	}

	public void setDescription3(String description3) {
		this.description3 = description3;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public Double getLargeur() {
		return largeur;
	}

	public void setLargeur(Double largeur) {
		this.largeur = largeur;
	}

	public Double getLongueur() {
		return longueur;
	}

	public void setLongueur(Double longueur) {
		this.longueur = longueur;
	}

	public Double getPrix() {
		return prix;
	}

	public void setPrix(Double prix) {
		this.prix = prix;
	}

}
