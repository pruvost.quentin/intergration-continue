package fr.univlittoral.javaquarium.DO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="U_UTILISATEUR")
public class UtilisateurDO {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="U_ID")
	private Integer id;

	@Column(name="U_PSEUDO", nullable=false)
	private String pseudo;

	@Column(name="U_PASSWORD", nullable=false)
	private PasswordDO password;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public PasswordDO getPassword() {
		return password;
	}

	public void setPassword(PasswordDO password) {
		this.password = password;
	}
	
}
